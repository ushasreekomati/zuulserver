package com.epam.ZuulService.filter;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;

@Component
public class PreFilter extends ZuulFilter{
	private static final int FILTER_ORDER =1;
	private static final String FILTER_TYPE ="pre";
	private static final boolean SHOULD_FILTER =true;


	@Override
	public boolean shouldFilter() {
		return SHOULD_FILTER;
	}

	@Override
	public Object run() throws ZuulException {
		System.out.println("Executing from Pre Filter with Filter Order"+ FILTER_ORDER);
		return null;
	}

	@Override
	public String filterType() {
		return FILTER_TYPE;
	}

	@Override
	public int filterOrder() {
		return FILTER_ORDER;
	}

}
